package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    private final Map<Cuisine,List<Customer>> cuisineMap;
    private final Map<Customer,List<Cuisine>> customerMap;
    
    public InMemoryCuisinesRegistry() {
    	cuisineMap = new HashMap<Cuisine, List<Customer>>();
    	customerMap = new HashMap<Customer, List<Cuisine>>();
	}

    @Override
    public void register(final Customer userId, final Cuisine cuisine) {
        
    	List<Customer> customerList;
    	if (cuisineMap.get(cuisine) != null) {
    		customerList = cuisineMap.get(cuisine);
    	} else {
    		customerList = new LinkedList<Customer>();
    		cuisineMap.put(cuisine, customerList);
    	}
    	customerList.add(userId);
    	
    	List<Cuisine> cuisineList;
    	if (customerMap.get(userId) != null) {
    		cuisineList = customerMap.get(userId);
    	} else {
    		cuisineList = new LinkedList<Cuisine>();
    		customerMap.put(userId, cuisineList);
    	}
    	cuisineList.add(cuisine);
    	
    }

    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {
    	return cuisineMap.get(cuisine);
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {
        return customerMap.get(customer);
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {
    	
    	Map<Integer, Cuisine> ordered = new TreeMap<Integer, Cuisine>(Collections.reverseOrder());
    	
    	for (Cuisine cuisine : cuisineMap.keySet()) {
    		ordered.put(cuisineMap.get(cuisine).size(), cuisine);
		}
    		
    	List<Cuisine> top = new LinkedList<Cuisine>();
    	
    	for (int key : ordered.keySet()) {
			top.add(ordered.get(key));
			
			if (top.size() == n) {
				break;
			}
		}

    	return top;	
    }
}

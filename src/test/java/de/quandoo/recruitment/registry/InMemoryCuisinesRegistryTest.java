package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

import java.util.LinkedList;
import java.util.List;

public class InMemoryCuisinesRegistryTest {

    private InMemoryCuisinesRegistry cuisinesRegistry = new InMemoryCuisinesRegistry();

    @Before
    public void init() {
    	Customer customer1 = new Customer("1");
    	cuisinesRegistry.register(customer1, new Cuisine("french"));
    	cuisinesRegistry.register(customer1, new Cuisine("german"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("2"), new Cuisine("french"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("italian"));
        cuisinesRegistry.register(new Customer("3"), new Cuisine("german"));
        cuisinesRegistry.register(new Customer("4"), new Cuisine("german"));
    }
    
    @Test
    public void testFrenchCuisine() {
    	List<Customer> expected = new LinkedList<Customer>();
    	expected.add(new Customer("1"));
    	expected.add(new Customer("2"));

        List<Customer> result = cuisinesRegistry.cuisineCustomers(new Cuisine("french"));
        
        assertArrayEquals(expected.toArray(), result.toArray());
    }

    @Test
    public void cuisineCustomersNullCheck() {
        assertNull(cuisinesRegistry.cuisineCustomers(null));
    }

    @Test
    public void customerCuisinesNullCheck() {
    	assertNull(cuisinesRegistry.customerCuisines(null));
    }
    
    @Test
    public void customerCuisinesSingleTest() {
    	assertTrue("german".equals(cuisinesRegistry.customerCuisines(new Customer("2")).get(0).getName()));
    }

    @Test
    public void customerCuisinesMultiTest() {
    	List<Cuisine> expected = new LinkedList<Cuisine>();
    	expected.add(new Cuisine("french"));
    	expected.add(new Cuisine("german"));
    	assertArrayEquals(expected.toArray(), cuisinesRegistry.customerCuisines(new Customer("1")).toArray());
    }
    
    @Test
    public void topTwoTest() {
    	List<Cuisine> expected = new LinkedList<Cuisine> ();
    	expected.add(new Cuisine("german"));
    	expected.add(new Cuisine("french"));
    	
        assertTrue((cuisinesRegistry.topCuisines(2).equals(expected)));
    }


}